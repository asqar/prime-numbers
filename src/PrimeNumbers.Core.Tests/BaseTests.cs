﻿using DryIoc;
using NUnit.Framework;

namespace PrimeNumbers.Core.Tests
{
    public abstract class BaseTests
    {
        protected IContainer _container;

        [TestFixtureSetUp]
        public void InitFixture()
        {
        }

        [TestFixtureTearDown]
        public void TearDownFixture()
        {
        }

        [SetUp]
        public virtual void Init()
        {
            _container = new Container();
            Register(_container);
        }

        [TearDown]
        public virtual void Cleanup()
        {
            _container?.Dispose();
        }

        public abstract void Register(IContainer container);
    }
}