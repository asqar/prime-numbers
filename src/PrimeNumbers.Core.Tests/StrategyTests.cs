﻿using System.Linq;
using System.Reflection;
using DryIoc;
using NUnit.Framework;
using PrimeNumbers.Core.Strategies;

namespace PrimeNumbers.Core.Tests
{
    [TestFixture]
    public class StrategyTests : BaseTests
    {
        public override void Register(IContainer container)
        {
            container.Register<BruteForceStrategy>();
            container.Register<ParallelBruteForceStrategy>();
            container.Register<SieveOfEratosthenesStrategy>();
            container.Register<SieveOfSundaramStrategy>();
            container.Register<SieveOfAtkinsStrategy>();
        }

        [Test]
        public void TestBruteForce()
        {
            var stategy = _container.Resolve<BruteForceStrategy>()
                ;
            var range = new CalcRange {From = 0, To = 1000};
            var numbersEnumerable = stategy.GetNumbers(range);
            var numbers = numbersEnumerable.ToList();
            Assert.That(numbers.Any(), Is.True);
        }

        [Test]
        public void TestParallelBruteForce()
        {
            var stategy = _container.Resolve<ParallelBruteForceStrategy>()
                ;
            var range = new CalcRange {From = 0, To = 1000};
            var numbersEnumerable = stategy.GetNumbers(range);
            var numbers = numbersEnumerable.ToList();
            Assert.That(numbers.Any(), Is.True);
        }

        [Test]
        public void TestSieveOfAtkins()
        {
            var stategy = _container.Resolve<SieveOfAtkinsStrategy>()
                ;
            var range = new CalcRange {From = 0, To = 1000};
            var numbersEnumerable = stategy.GetNumbers(range);
            var numbers = numbersEnumerable.ToList();
            Assert.That(numbers.Any(), Is.True);
        }

        [Test]
        public void TestSieveOfEratosthenes()
        {
            var stategy = _container.Resolve<SieveOfEratosthenesStrategy>()
                ;
            var range = new CalcRange {From = 0, To = 1000};
            var numbersEnumerable = stategy.GetNumbers(range);
            var numbers = numbersEnumerable.ToList();
            Assert.That(numbers.Any(), Is.True);
        }

        [Test]
        public void TestSieveOfSundaram()
        {
            var stategy = _container.Resolve<SieveOfSundaramStrategy>()
                ;
            var range = new CalcRange {From = 0, To = 1000};
            var numbersEnumerable = stategy.GetNumbers(range);
            var numbers = numbersEnumerable.ToList();
            Assert.That(numbers.Any(), Is.True);
        }
    }
}