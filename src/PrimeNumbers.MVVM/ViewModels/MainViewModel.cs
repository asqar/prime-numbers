﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.WeakSubscription;
using PrimeNumbers.Core;

namespace PrimeNumbers.MVVM.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        public class StrategySwitch : MvxViewModel
        {
            private bool _selected;

            public StrategyTypeEnum Type { get; set; }
            public bool Selected {
                get { return _selected; }
                set
                {
                    _selected = value;
                    RaisePropertyChanged(() => Selected);
                }
            }
            public string Title { get; set; }
        }

        private int _upToNumber;
        private readonly IApplicationManager _applicationManager;
        private Task _mainTask;

        public IOutput Output { get; set; }

        public int UpToNumber {
            get { return _upToNumber; }
            set
            {
                _upToNumber = value;
                RaisePropertyChanged(() => UpToNumber);
                RaisePropertyChanged(() => StartButtonEnabled);
            }
        }

        public INumberList NumberList { get; set; }

        public List<StrategySwitch> Strategies { get; }

        public bool StartButtonEnabled
        {
            get
            {
                return (_mainTask == null || _mainTask.IsCanceled
                    || _mainTask.IsCompleted || _mainTask.IsFaulted)
                       && Strategies.Any(s => s.Selected) && UpToNumber > 3;
            }
        }

        public ICommand StartCommand
        {
            get
            {
                return new MvxCommand(() =>
                {
                    if (!StartButtonEnabled)
                        return;
                    _mainTask = _applicationManager.Run(new CalculationOptions()
                    {
                        Range = new CalcRange() {From = 2, To = UpToNumber},
                        UseStrategies = Strategies.Where(s => s.Selected).Select(s => s.Type).ToList()
                    });
                    RaisePropertyChanged(() => StartButtonEnabled);
                    _mainTask.ContinueWith((o) => RaisePropertyChanged(() => StartButtonEnabled));
                });
            }
        }

        private string[] SplitCamelCase(string source)
        {
            return Regex.Split(source, @"(?<!^)(?=[A-Z])");
        }

        public MainViewModel(IApplicationManager applicationManager, INumberList numberList, IOutput output)
        {
            _applicationManager = applicationManager;

            UpToNumber = 10000;
            Output = output;
            //Output.PropertyChanged += (sender, args) =>
            //{
            //    RaisePropertyChanged(() => Output);
            //};

            NumberList = numberList;
            NumberList.CollectionChanged += (sender, args) => RaisePropertyChanged(() => NumberList);
            Strategies = new List<StrategySwitch>();
            foreach (StrategyTypeEnum strategyType in Enum.GetValues(typeof(StrategyTypeEnum)))
            {
                var title = Enum.GetName(typeof (StrategyTypeEnum), strategyType);
                title = string.Join(" ", SplitCamelCase(title));
                var s = new StrategySwitch
                {
                    Type = strategyType,
                    Title = $"{ title } strategy",
                };
                s.PropertyChanged += (sender, args) => RaisePropertyChanged(() => StartButtonEnabled);
                Strategies.Add(s);
            }
        }
    }
}
