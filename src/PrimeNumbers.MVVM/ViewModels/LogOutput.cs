﻿using MvvmCross.Core.ViewModels;
using PrimeNumbers.Core;

namespace PrimeNumbers.MVVM.ViewModels
{
    public class LogOutput : MvxViewModel, IOutput
    {
        private string _text;

        public string Text {
            get { return _text; }
            set
            {
                _text = value;
                RaisePropertyChanged(() => Text);
            }
        }

        public LogOutput()
        {
            _text = string.Empty;
        }

        public void WriteLine(string content)
        {
            Text += string.Format($"{content}\n");
        }

        public void Clear()
        {
            Text = string.Empty;
        }
    }
}