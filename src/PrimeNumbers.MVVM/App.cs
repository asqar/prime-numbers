﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using PrimeNumbers.Core;
using PrimeNumbers.Core.Strategies;
using PrimeNumbers.MVVM.ViewModels;

namespace PrimeNumbers.MVVM
{
    public class App : MvxApplication
    {
        public App()
        {
            Mvx.RegisterSingleton<IMvxAppStart>(new MvxAppStart<MainViewModel>());

            // if we can use reflection
            typeof(IStrategy).GetTypeInfo().Assembly.CreatableTypes()
                .InNamespace("PrimeNumbers.Core.Strategies")
                .AsTypes()
                .RegisterAsLazySingleton();
            // otherwise do it manually
            //Mvx.RegisterType<BruteForceStrategy, BruteForceStrategy>();
            //Mvx.RegisterType<ParallelBruteForceStrategy, ParallelBruteForceStrategy>();
            //Mvx.RegisterType<SieveOfEratosthenesStrategy, SieveOfEratosthenesStrategy>();
            //Mvx.RegisterType<SieveOfSundaramStrategy, SieveOfSundaramStrategy>();
            //Mvx.RegisterType<SieveOfAtkinsStrategy, SieveOfAtkinsStrategy>();

            var output = new LogOutput();
            Mvx.RegisterSingleton(typeof(IOutput), output);

            var numberList = new NumberList();
            Mvx.RegisterSingleton<INumberList>(numberList);
            Mvx.RegisterType<IOutput, LogOutput>();
            Mvx.RegisterType<IApplicationManager, ApplicationManager>();
            Mvx.RegisterType<IStrategyFactory, MvxStrategyFactory>();
        }
    }
}
