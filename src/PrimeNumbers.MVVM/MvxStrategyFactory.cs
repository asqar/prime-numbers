﻿using System;
using MvvmCross.Platform;
using PrimeNumbers.Core;
using PrimeNumbers.Core.Strategies;

namespace PrimeNumbers.MVVM
{
    public class MvxStrategyFactory : IStrategyFactory
    {
        public IStrategy Create(StrategyTypeEnum strategyType)
        {
            switch (strategyType)
            {
                case StrategyTypeEnum.BruteForce:
                    return Mvx.Resolve<BruteForceStrategy>();
                case StrategyTypeEnum.ParallelBruteForce:
                    return Mvx.Resolve<ParallelBruteForceStrategy>();
                case StrategyTypeEnum.SieveOfEratosthenes:
                    return Mvx.Resolve<SieveOfEratosthenesStrategy>();
                case StrategyTypeEnum.SieveOfSundaram:
                    return Mvx.Resolve<SieveOfSundaramStrategy>();
                case StrategyTypeEnum.SieveOfAtkins:
                    return Mvx.Resolve<SieveOfAtkinsStrategy>();
                default:
                    throw new ArgumentException();
            }
        }
    }
}
