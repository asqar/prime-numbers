﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PrimeNumbers.Core
{
    public interface IStrategyFactory
    {
        IStrategy Create(StrategyTypeEnum strategyType);
    }

    public enum StrategyTypeEnum
    {
        BruteForce = 1,
        ParallelBruteForce = 2,
        SieveOfEratosthenes = 3,
        SieveOfSundaram = 4,
        SieveOfAtkins = 5
    }

    public struct CalcRange
    {
        public int From { get; set; }
        public int To { get; set; }
    }

    public struct CalculationOptions
    {
        public CalcRange Range { get; set; }
        public IList<StrategyTypeEnum> UseStrategies { get; set; }
    }

    public interface IApplicationManager
    {
        Task Run(CalculationOptions options);
    }
    public class ApplicationManager : IApplicationManager
    {
        private readonly IOutput _output;
        private readonly ILogger _logger;
        private readonly INumberList _numberList;
        private readonly IStrategyFactory _strategyFactory;
        private readonly SynchronizationContext _syncContext;

        public ApplicationManager(
            INumberList numberList,
            IOutput output,
            ILogger logger,
            IStrategyFactory strategyFactory)
        {
            // we assume this ctor is called from the UI thread!
            _syncContext = SynchronizationContext.Current;

            _numberList = numberList;
            _output = output;
            _strategyFactory = strategyFactory;
            _logger = logger;
        }

        /// <summary>
        /// Start calculation of prime numbers
        /// </summary>
        public async Task Run(CalculationOptions options)
        {
            _numberList.Clear();
            _output.Clear();

            var tasks = options.UseStrategies.Select(strategyType =>
                Task.Factory.StartNew(() =>
                {
                    var numbers = _strategyFactory.Create(strategyType).GetNumbers(options.Range);
                    foreach (var number in numbers)
                    {
                        if (_syncContext != null)
                        {
                            _syncContext.Post(o => { _numberList.Add(number); }, null);
                        }
                        else
                        {
                            _output.WriteLine($"{number}");
                        }
                        _logger.WriteLine("{0}: {1}", strategyType, number);
                    }
                })).ToList();
            var t = Task.WhenAll(tasks);
            await t;
        }
    }
}
