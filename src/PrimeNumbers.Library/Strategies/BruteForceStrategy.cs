﻿using System;
using System.Collections.Generic;

namespace PrimeNumbers.Core.Strategies
{
	public class BruteForceStrategy : StrategyBase
    {
		public override IEnumerable<int> GetNumbers(CalcRange range)
		{
			for (var i = range.From; i <= range.To; i++)
			{
				if (IsPrime(i))
				{
				    yield return i;
				}
			}
		}

		public static bool IsPrime(int n)
		{
			if ((n & 1) == 0)
			{
			    return n == 2;
			}
			for (int i = 3; (i * i) <= n; i += 2)
			{
				if ((n % i) == 0)
				{
					return false;
				}
			}
			return n != 1;
		}
    }
}
