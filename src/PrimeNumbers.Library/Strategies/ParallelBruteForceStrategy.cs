﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PrimeNumbers.Core.Strategies
{
	public class ParallelBruteForceStrategy : StrategyBase
    {
        public override IEnumerable<int> GetNumbers(CalcRange range)
        {
	        var integerNumbersList = Enumerable.Range(range.From, range.To);
            const int coreCount = 4;
	        var primeNumbers = integerNumbersList.AsParallel().WithDegreeOfParallelism(coreCount).Where(IsPrime);
            return primeNumbers;
        }

	    public static bool IsPrime(int n)
        {
            if ((n & 1) == 0)
            {
                return n == 2;
            }
            for (int i = 3; (i * i) <= n; i += 2)
            {
                if ((n % i) == 0)
                {
                    return false;
                }
            }
            return n != 1;
        }
    }
}
