﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace PrimeNumbers.Core.Strategies
{
    /// <summary>
    /// Taken from CodeProject article:
    /// https://www.codeproject.com/Articles/490085/Eratosthenes-Sundaram-Atkins-Sieve-Implementation
    /// </summary>
    public class SieveOfSundaramStrategy : StrategyBase
    {
        public override IEnumerable<int> GetNumbers(CalcRange range)
        {
            return FindPrimeUsingSieveOfSundaram(range.To);
		}

        private static IEnumerable<int> FindPrimeUsingSieveOfSundaram(int topCandidate)
        {
            yield return 2;

            var k = topCandidate / 2;
            var myBA1 = new BitArray(k + 1);

            /* SET ALL TO PRIME STATUS */
            myBA1.SetAll(true);

            /* SEIVE */
            for (var i = 1; i < k; i++)
            {
                var denominator = (i << 1) + 1;
                var maxVal = (k - i) / denominator;
                for (var j = i; j <= maxVal; j++)
                {
                    myBA1[i + j * denominator] = false;
                }
            }
            for (var i = 1; i < k; i++)
            {
                if (myBA1[i])
                {
                    var primeNumber = (i << 1) + 1;
                    yield return primeNumber;
                }
            }
        }
    }
}
