﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace PrimeNumbers.Core.Strategies
{
    /// <summary>
    /// Taken from CodeProject article:
    /// https://www.codeproject.com/Articles/490085/Eratosthenes-Sundaram-Atkins-Sieve-Implementation
    /// </summary>
    public class SieveOfAtkinsStrategy : StrategyBase
    {
        public override IEnumerable<int> GetNumbers(CalcRange range)
        {
            yield return 2;
            yield return 3;
            var primeNumbers = FindPrimeUsingSieveOfAtkins(range.To);
            for (var i = range.From; i <= range.To; i++)
            {
                if (primeNumbers[i])
                    yield return i;
            }
		}

        private static BitArray FindPrimeUsingSieveOfAtkins(int topCandidate)
        {
            var isPrime = new BitArray(topCandidate + 1);

            var squareRoot = (int)Math.Sqrt(topCandidate);

            int xSquare = 1, xStepsize = 3;

            for (var x = 1; x <= squareRoot; x++)
            {
                var ySquare = 1;
                var yStepsize = 3;
                for (var y = 1; y <= squareRoot; y++)
                {
                    var computedVal = (xSquare << 2) + ySquare;

                    if ((computedVal <= topCandidate) && (computedVal % 12 == 1 || computedVal % 12 == 5))
                        isPrime[computedVal] = !isPrime[computedVal];

                    computedVal -= xSquare;
                    if ((computedVal <= topCandidate) && (computedVal % 12 == 7))
                        isPrime[computedVal] = !isPrime[computedVal];

                    if (x > y)
                    {
                        computedVal -= ySquare << 1;
                        if ((computedVal <= topCandidate) && (computedVal % 12 == 11))
                            isPrime[computedVal] = !isPrime[computedVal];
                    }
                    ySquare += yStepsize;
                    yStepsize += 2;
                }
                xSquare += xStepsize;
                xStepsize += 2;
            }

            for (var n = 5; n <= squareRoot; n++)
            {
                if (!isPrime[n]) continue;
                var k = n * n;
                for (var z = k; z <= topCandidate; z += k)
                    isPrime[z] = false;
            }

            return isPrime;
        }
    }
}
