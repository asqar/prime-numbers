﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace PrimeNumbers.Core.Strategies
{
    /// <summary>
    /// Taken from CodeProject article:
    /// https://www.codeproject.com/Articles/490085/Eratosthenes-Sundaram-Atkins-Sieve-Implementation
    /// </summary>
    public class SieveOfEratosthenesStrategy : StrategyBase
    {
        public override IEnumerable<int> GetNumbers(CalcRange range)
        {
            return FindPrimeUsingSieveOfEratosthenes(range.To);
		}

        private static IEnumerable<int> FindPrimeUsingSieveOfEratosthenes(int topCandidate)
        {
            var myBA1 = new BitArray(topCandidate + 1);

            /* Set all but 0 and 1 to prime status */
            myBA1.SetAll(true);
            myBA1[0] = myBA1[1] = false;

            /* Mark all the non-primes */
            var thisFactor = 2;
            var lastSquare = 0;

            while (thisFactor * thisFactor <= topCandidate)
            {
                /* Mark the multiples of this factor */
                int mark = thisFactor + thisFactor;
                while (mark <= topCandidate)
                {
                    myBA1[mark] = false;
                    mark += thisFactor;

                }

                /* Print the proven primes so far */
                var thisSquare = thisFactor * thisFactor;
                for (; lastSquare < thisSquare; lastSquare++)
                {
                    if (myBA1[lastSquare])
                        yield return lastSquare;
                }

                /* Set thisfactor to next prime */
                thisFactor++;
                while (!myBA1[thisFactor]) { thisFactor++; }

            }
            /* Print the remaining primes */
            for (; lastSquare <= topCandidate; lastSquare++)
            {
                if (myBA1[lastSquare])
                    yield return lastSquare;
            }
        }
    }
}
