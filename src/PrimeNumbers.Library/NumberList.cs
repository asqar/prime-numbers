﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace PrimeNumbers.Core
{
    public interface INumberList : IList<int>, INotifyCollectionChanged
    {
    }

    public class NumberList : ObservableCollection<int>, INumberList
    {
    }
}
