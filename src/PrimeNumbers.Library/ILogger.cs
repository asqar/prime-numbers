﻿using System;
namespace PrimeNumbers.Core
{
	public interface ILogger
	{
		void WriteLine(string format, params object[] args);
	}
}
