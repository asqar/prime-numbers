﻿using System;
using System.ComponentModel;

namespace PrimeNumbers.Core
{
	public interface IOutput
	{
		void WriteLine(string content);
	    void Clear();
	}
}
