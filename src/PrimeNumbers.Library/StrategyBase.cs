﻿using System.Collections.Generic;

namespace PrimeNumbers.Core
{
    public interface IStrategy
    {
        IEnumerable<int> GetNumbers(CalcRange range);
    }

    /// <summary>
    /// Please inherit IStrategy class from this one
    /// </summary>
	public abstract class StrategyBase : IStrategy
    {
        public abstract IEnumerable<int> GetNumbers(CalcRange range);
    }
}
