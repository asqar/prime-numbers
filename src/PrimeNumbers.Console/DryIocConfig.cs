﻿using DryIoc;
using PrimeNumbers.Core;
using PrimeNumbers.Core.Strategies;

namespace PrimeNumbers.Console
{
    public class DryIocConfig
    {
 
        public static IContainer CreateContainer()
        {
            var container = new Container();
            container.RegisterInstance<ILogger>(new NLogger(NLogger.CreateLogger()));
            container.Register<IOutput, ConsoleOutput>(new SingletonReuse());
            container.Register<IApplicationManager, ApplicationManager>();
            container.Register<INumberList, NumberList>();

            container.Register<IStrategyFactory, StrategyFactory>();
            container.Register<IStrategy, BruteForceStrategy>(serviceKey: StrategyTypeEnum.BruteForce);
            container.Register<IStrategy, ParallelBruteForceStrategy>(serviceKey: StrategyTypeEnum.ParallelBruteForce);
            container.Register<IStrategy, SieveOfEratosthenesStrategy>(serviceKey: StrategyTypeEnum.SieveOfEratosthenes);
            container.Register<IStrategy, SieveOfSundaramStrategy>(serviceKey: StrategyTypeEnum.SieveOfSundaram);
            container.Register<IStrategy, SieveOfAtkinsStrategy>(serviceKey: StrategyTypeEnum.SieveOfAtkins);

            return container;
        }
    }
}