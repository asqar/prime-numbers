﻿using System;
using DryIoc;
using PrimeNumbers.Core;

namespace PrimeNumbers.Console
{
    public class StrategyFactory : IStrategyFactory
    {
        private readonly IContainer _container;

        public StrategyFactory(IContainer container)
        {
            _container = container;
        }

        public IStrategy Create(StrategyTypeEnum strategyType)
        {
            switch (strategyType)
            {
                case StrategyTypeEnum.BruteForce:
                    return _container.Resolve<IStrategy>(StrategyTypeEnum.BruteForce);
                case StrategyTypeEnum.ParallelBruteForce:
                    return _container.Resolve<IStrategy>(StrategyTypeEnum.ParallelBruteForce);
                case StrategyTypeEnum.SieveOfEratosthenes:
                    return _container.Resolve<IStrategy>(StrategyTypeEnum.SieveOfEratosthenes);
                case StrategyTypeEnum.SieveOfSundaram:
                    return _container.Resolve<IStrategy>(StrategyTypeEnum.SieveOfSundaram);
                case StrategyTypeEnum.SieveOfAtkins:
                    return _container.Resolve<IStrategy>(StrategyTypeEnum.SieveOfAtkins);
                default:
                    throw new ArgumentException();
            }
        }
    }
}