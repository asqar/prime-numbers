﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DryIoc;
using PrimeNumbers.Core;

namespace PrimeNumbers.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (ValidateArgs(args)) return;

            System.Console.WriteLine("Starting tasks...");
            MainAsync(args).Wait();
            System.Console.WriteLine("Done!");
            System.Console.WriteLine("Please press any key to close the program...");
            System.Console.Read();
        }

        private static bool ValidateArgs(string[] args)
        {
            if (args.Length == 1)
            {
                var i = 1;
                System.Console.WriteLine("The following methods are implemented:");
                foreach (var service in Enum.GetNames(typeof (StrategyTypeEnum)))
                {
                    System.Console.WriteLine($"{i}. {service}.");
                    i++;
                }
            }
            if (args.Length < 2)
            {
                var fullName = Assembly.GetEntryAssembly().Location;
                var myName = Path.GetFileNameWithoutExtension(fullName);
                System.Console.WriteLine(
                    $"Please pass N number and numbers of methods (whitespace-separated), for example:\n{myName} 1000 1 2 3 4 5 6"
                    );
                return true;
            }
            var argNumbers = ParseArgs(args);
            if (argNumbers.Count == 0)
                return true;

            return false;
        }

        private static List<int> ParseArgs(string[] args)
        {
            var argNumbers = new List<int>();
            foreach (var arg in args)
            {
                int i;
                if (int.TryParse(arg, out i))
                {
                    argNumbers.Add(i);
                }
                else
                {
                    System.Console.WriteLine("Please provide integer arguments");
                }
            }
            return argNumbers;
        }

        private static async Task MainAsync(string[] args)
        {
            var argNumbers = ParseArgs(args);
            var n = argNumbers[0];
            var strategiesSelected = argNumbers.Skip(1);

            using (var container = DryIocConfig.CreateContainer())
            {
                var app =
                    container.Resolve<IApplicationManager>();
                await app.Run(new CalculationOptions
                {
                    UseStrategies = strategiesSelected.Select(s => (StrategyTypeEnum) s).ToList(),
                    Range = new CalcRange {From = 2, To = n}
                });
            }
        }
    }
}