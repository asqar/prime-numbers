﻿using PrimeNumbers.Core;

namespace PrimeNumbers.Console
{
    public class ConsoleOutput : IOutput
    {
        public void WriteLine(string content)
        {
            System.Console.WriteLine(content);
        }

        public void Clear()
        {
            //System.Console.Clear();
        }
    }
}