﻿using System;
using System.Linq;
using DryIoc;
using log4net;
using NLog;
using NLog.Config;
using ILogger = PrimeNumbers.Core.ILogger;
using LogManager = NLog.LogManager;

namespace PrimeNumbers.Console
{
    public class NLogger : ILogger
    {
        private readonly Logger _logger;

        public static Logger CreateLogger()
        {
            LogManager.Configuration = new XmlLoggingConfiguration("NLog.config");
            var logger = LogManager.GetLogger("Prime.Numbers");
            return logger;
        }

        public NLogger(Logger logger)
        {
            _logger = logger;
        }

        public void WriteLine(string format, params object[] args)
        {
            _logger.Debug(format, args);
        }
    }
}