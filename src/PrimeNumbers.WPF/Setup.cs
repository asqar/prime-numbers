﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Wpf.Platform;
using MvvmCross.Wpf.Views;
using NLog.Config;
using PrimeNumbers.Core;
using PrimeNumbers.MVVM.ViewModels;

namespace PrimeNumbers.WPF
{
    public class Setup : MvxWpfSetup
    {
        public Setup(Dispatcher uiThreadDispatcher, IMvxWpfViewPresenter presenter) : base(uiThreadDispatcher, presenter)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            NLog.LogManager.Configuration = new XmlLoggingConfiguration("NLog.config");
            var logger = NLog.LogManager.GetCurrentClassLogger();
            var nlogger = new NLogger(logger);
            Mvx.RegisterSingleton(typeof(ILogger), nlogger);

            return new PrimeNumbers.MVVM.App();
        }
    }
}
