﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrimeNumbers.Core;

namespace PrimeNumbers.WPF
{
    public class NLogger : ILogger
    {
        private readonly NLog.Logger _logger;

        public NLogger(NLog.Logger logger)
        {
            _logger = logger;
        }

        public void WriteLine(string format, params object[] args)
        {
            _logger.Debug(string.Format(format, args));
        }
    }
}
